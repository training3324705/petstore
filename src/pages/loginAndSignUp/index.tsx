import React, { useState } from "react";
import Login from "../../components/Login";
import SignUp from "../../components/SignUp";
import styled from "styled-components";

function LoginAndSignUp() {
  const [show, setShow] = useState(true);
  return (
    <MainWrapper>
      {show ? <Login setShow={setShow} /> : <SignUp setShow={setShow} />}
    </MainWrapper>
  );
}

export default LoginAndSignUp;

// styles
const MainWrapper = styled.div`
  /* background-image: url("https://images.unsplash.com/photo-1573865526739-10659fec78a5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8Y2F0fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=1000&q=1500");
  background-repeat: no-repeat; */
`;
